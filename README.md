# bibliography

This repository contains the complete bibliography for the thesis and all the other related documents, publications, slides and so on. The repositories in this group should use this repository as submodule in order to always have access to the latest bibliography.
